# New cool Eukaryotic genomes and how to merge it in broaddb

## Fill metadata

The `euka_genomes.csv` metadata file has 7 columns: ID (CUS00001 with incremental progress), Species, Fasta location, Taxonomic lineage (if possible), Paper, Data source, notes. Please, be **very** careful when adding the taxonomy as it should not clash with other eukaryotes, maybe a good idea to grep your lineage here `/gpfs/projects/bsc40/current/gmutti/projects/repdb/results/taxonomies/all_eukaryotes.tsv` to see if there are already close genomes.

## Prepare the files

You need to fill the metadata file and for each proteome add a file in data/proteomes/$id.fa (please here and with this name for now!). Then you can run the pipeline that will do some QC, busco etc. `snakemake -j4` will suffice.

## Add to repdb

Then follow the instructions in the RepDB README, as `euka_genomes.csv` will be updated snakemake will detect it and redo all necessary steps.

## To add

* [Bygyromonadea](https://www.sciencedirect.com/science/article/pii/S1055790322000811?via%3Dihub) very interesting clade!!!
* [Dinoflagellates](https://doi.org/10.1093/pnasnexus/pgac202): Understand if they are worth it
* [Euglenids](https://onlinelibrary.wiley.com/doi/10.1111/jeu.12973)
* [Apicomplexans](https://doi.org/10.1093/molbev/msad002): Maybe not necessary
* [Blastocladiella emersonii](https://doi.org/10.1093/gbe/evac157): Maybe not needed
* [Meteora sporadica](https://doi.org/10.1016/j.cub.2023.12.032): onlye reads are available!! This would be a very cool genome

## Notes

* For [Provora](https://doi.org/10.1038/s41586-022-05511-5) I got the filtered protein sets and did the taxonomy manually. For now the taxonomy collides with the one reported for *A. twista* in EukProt!! 

## TODO

* maybe add a strategy column
* Implement omark in pipeline
